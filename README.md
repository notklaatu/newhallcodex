# Newhall Bestiary

I subscribe to [Geek Archaeology](http://geekarchaeology.com) for its amazing monsters and factions and other products of world-building. You should too.

However, when I game I prefer to have those resources as an epub on my ereader.

This repo contains content from *Geek Archaeology*, copyright by Brent Newhall and licensed under a Creative Commons license. It contains his monsters and factions placed into [dmschema](https://gitlab.com/notklaatu/dmschema) XML for easy EPUB or PDF export.

If you like what you see here, you should thank [Brent Newhall](http://brentnewhall.com). All I did was steal his work and wrap it in XML.


## Using these stylesheets

This has only been tested on Linux and BSD, but as long as you know Docbook, you should be able to use this with only modest changes to the GNUmakefile.
      
*Assuming you're using Linux or BSD, the required software is probably available from your software repository or ports tree.*

Requirements:

* [Docbook](http://docbook.org)
* [Junction](https://www.theleagueofmoveabletype.com/junction) font
* [Andada](http://www.1001fonts.com/andada-font.html)
* [TeX Gyre Bonum](http://www.1001fonts.com/tex-gyre-bonum-font.html) (bundled in this repo to fix render errors)
* [xmlto](https://pagure.io/xmlto)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)
* [Image Magick](http://imagemagick.org/script/index.php)

A valid alternative to ``pdftk`` is [stapler](https://pypi.python.org/pypi/stapler/0.3.3).

A valid alternative to Image Magick is [Graphics Magick](http://www.graphicsmagick.org/), but you'll have to alias its commands so that they don't rely on the ``gm`` prefix: ``gm convert`` must be aliased to ``convert``, and so on.

Optional, to avoid warnings about not having a **symbol** font installed:

* [UniCons](https://fontlibrary.org/en/font/unicons)


### Setup

The font **TeX Gyre Bonum** does not function properly because it's OTF rather than TTF.

I have converted the source OTF to TTF for you. They're included with this repository.

Place them in your ``$HOME/.local/share/fonts/t/`` directory so Docbook can detect them.


### GNUmakefile

The GNUmakefile builds a PDF, the standard delivery format for most indie RPG adventures. You can edit inline parameters to suit your needs.

Notably:

* paper size is A4 by default
* license is set to ``no`` by default, which renders the standard *for personal use only* footer. You can alternately set it to ``sa`` for a Creative Commons BY-SA license message in the footer, instead. The Open Game License only covers mechanics, so your CC license only applies to your *story content*
* the name of the output file is ``example.pdf`` by default

Once you've written your adventure and are ready to build:

    $ make clean pdf

It also builds to EPUB, which is, unlike PDF, an actual ebook format.

    $ make clean epub

The output is placed into a directory called ``dist``.


## Bugs

This is a work in progress and a labour of love. I anxiously follow Brent's updates to his blog, but I only enter them here when convenient, and I probably won't rush to update these entries if he goes and changes a bunch of stuff. In fact, I might change stuff if, after some playtesting, I find something that works better for me.

In other words, Brent Newhall's work is his own, and the bugs in this repo are mine.


## License

It's all open source and free culture. See LICENSE file for details.